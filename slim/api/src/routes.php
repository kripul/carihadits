<?php
// Routes

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
// Retrieve bukhari with id
    $app->get('/bukhari/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_bukhari WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari bukhari by keyword

 $app->get('/bukhari/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_bukhari where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve abudaud with id
    $app->get('/abudaud/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_abudaud WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari abudaud by keyword

 $app->get('/abudaud/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_abudaud where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve ahmad with id
    $app->get('/ahmad/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_ahmad WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari ahmad by keyword

 $app->get('/ahmad/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_ahmad where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve darimi with id
    $app->get('/darimi/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_darimi WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari darimi by keyword

 $app->get('/darimi/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_darimi where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve ibnumajah with id
    $app->get('/ibnumajah/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_ibnumajah WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari ibnumajah by keyword

 $app->get('/ibnumajah/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_ibnumajah where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve malik with id
    $app->get('/malik/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_malik WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari malik by keyword

 $app->get('/malik/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_malik where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve muslim with id
    $app->get('/muslim/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_muslim WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari muslim by keyword

 $app->get('/muslim/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_muslim where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve nasai with id
    $app->get('/nasai/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_nasai WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari nasai by keyword

 $app->get('/nasai/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_nasai where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 // Retrieve tirmidzi with id
    $app->get('/tirmidzi/[{id}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_tirmidzi WHERE NoHdt=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $todos = $sth->fetchObject();
        return $this->response->withJson($todos);
    });
// cari tirmidzi by keyword

 $app->get('/tirmidzi/q/[{query}]', function ($request, $response, $args) {
        $sth = $this->db->prepare("SELECT * FROM had_tirmidzi where Isi_Indonesia LIKE :query or Isi_Arab_Gundul LIKE :query");
        $query = "%".$args['query']."%";
        $sth->bindParam("query", $query);
        $sth->execute();
        $todos = $sth->fetchAll();
        return $this->response->withJson($todos);
    });

 